# armv7_bootstrap
QEMU ARMv7 bare metal examples aimed at bootstrapping an OS kernel

main repo: https://gitlab.com/DavidDiPaola/armv7_bootstrap

## building and running
1. build: `make`
2. run an example: `./run.sh 00_helloworld/kernel.elf`
    - press Ctrl-A, then press X to exit QEMU
3. debug a running example: `./debugger.sh 00_helloworld/kernel.elf`

## boot process overview
1. the system resets
2. the CPU runs the kernel executable, which starts with the contents of `common/init.S`
3. the first instruction in `common/init.S` jumps to its reset handler
    - these first 8 instructions also conveniently serve as the vector table
4. the reset handler sets up the CPU
    1. tells the CPU to use the vector table in `common/init.S`
    2. sets the stack pointer register in each CPU mode (stack areas are defined in `common/init.S`)
5. finally, the reset handler jumps to the `main()` function

## TODOs
### done
- hello world
- exception/vector handler
- timer
- interrupt handler
- basic MMU setup, fault handler
- audio
### not done
- basic monitor/shell
- storage/disk/sdcard (raw, no filesystem)
- USB keyboard, mouse
- video
- networking?
- multiple CPU cores?

2018,2019,2020 David DiPaola.
Licensed under CC0 (public domain).
