/*
2019,2020 David DiPaola
Licensed under CC0 (public domain)
*/

#include "../common/inttypes.h"

#include "../common/uart.h"

#include "../common/arm.h"

#include "../config_code.h"

/* bit fields:
	ns (not secure):
	ng (not global):
	s (shareable):
	tex ():
	ap (access permissions):
		value    PL1 access    unprivileged access
		0b011    read,write    read,write
		(see DDI0406C.D table B3-8)
	domain ():
	xn (execute never): 0=can execute, 1=execution causes permission fault (see DDI0406C.D B3.7.2)
	c ():
	b ():
	pxn (privelaged execute never): 0=PL1 can execute, 1=execution causes permission fault (see DDI0406C.D B3.7.2)
*/

/* translation table level 1 entry macros (see: DDI0406C.D figure B3-4) */
#define _tt_l1_INVALID ((u32)( 0b00 ))
#define _tt_l1_PAGETABLE(address, domain, sbz, ns, pxn) ((u32)( \
	((address) & 0xFFFFFC00) | \
	(((domain)  &      0xF) <<  5) | \
	(((sbz)     &        1) <<  4) | \
	(((ns)      &        1) <<  3) | \
	(((pxn)     &        1) <<  2) | \
	(0b10                   <<  0)   \
))
#define _tt_l1_SECTION(address, ns, ng, s, tex, ap, domain, xn, c, b, pxn) ((u32)( \
	((address) & 0xFFF00000)    | \
	(((ns)      &     1) << 19) | \
	(((ng)      &     1) << 17) | \
	(((s)       &     1) << 16) | \
	((((ap)>>2) &     1) << 15) | \
	(((tex)     &   0x7) << 12) | \
	(((ap)      &   0x3) << 10) | \
	(((domain)  &   0xF) <<  5) | \
	(((xn)      &     1) <<  4) | \
	(((c)       &     1) <<  3) | \
	(((b)       &     1) <<  2) | \
	(1                   <<  1) | \
	(((pxn)     &     1) <<  0)   \
))

/* translation table level 2 entry macros (see: DDI0406C.D figure B3-5) */
#define _tt_l2_entry_INVALID ((u32)( 0b00 ))
#define _tt_l2_entry_PAGE_4K(address, ng, s, ap, tex, c, b, xn) ((u32)( \
	(((address)   & 0xFFFFF) << 12) | \
	(((ng)        &       1) << 11) | \
	(((s)         &       1) << 10) | \
	((((ap) >> 2) &       1) <<  9) | \
	(((tex)       &     0x7) <<  6) | \
	(((ap)        &     0x3) <<  4) | \
	(((c)         &       1) <<  3) | \
	(((b)         &       1) <<  2) | \
	(1                       <<  1) | \
	(((xn)        &       1) <<  0)   \
))

#define _tt_l1_type u32
#define _tt_l1_size (16384)
#define _tt_l1_length (_tt_l1_size / sizeof(_tt_l1_type))
static _tt_l1_type __attribute__((aligned(_tt_l1_size))) _tt_l1[_tt_l1_length];

static void
mmu_tt_l1_clear(void) {
	for (u32 i=0; i<_tt_l1_length; i++) {
		_tt_l1[i] = _tt_l1_INVALID;
	}
}

static u32
mmu_tt_l1_mapsection(u32 physaddr, u32 virtaddr) {
	/* TODO add args to set mapping flags */
	virtaddr &= 0xFFF00000;
	u32 entry = _tt_l1_SECTION(
		/*address*/ physaddr,
		/*ns*/ 0,  /* use the secure state's address map */
		/*ng*/ 0,  /* global */
		/*s*/ 1,  /* ignored because this mapping is strongly-ordered */
		/*tex*/ 0b000,  /* strongly-ordered & shareable (see: DDI0406C.D table B3-10) */
		/*ap*/ 0b011,  /* read & write access (see: DDI0406C.D table B3-8) */
		/*domain*/ 0b0000,  /* which DACR bits to check */
		/*xn*/ 0,  /* execution allowed */
		/*c*/ 0,  /* strongly-ordered & shareable (see: DDI0406C.D table B3-10) */
		/*b*/ 0,  /* strongly-ordered & shareable (see: DDI0406C.D table B3-10) */
		/*pxn*/ 0  /* privelaged execution allowed */
	);
	u32 index = (virtaddr >> 20) & 0xFFF;
	_tt_l1[index] = entry;

	return virtaddr;
}

static void
mmu_init(void) {
	u32 sctlr;

	uart_print("translation table level 1 address (tt l1): ");
	uart_print_hex32((u32)_tt_l1);
	uart_print_nl();

	/* disable MMU */
	arm_cp15_sctlr_get(sctlr);
	sctlr &= ~(1 << 0);
	arm_cp15_sctlr_set(sctlr);

	/* disable EAE/LPAE (use short descriptor mode), set TTBR0's table size to 16KB (also disables TTBR1) (see: DDI0406C.D figure B3-6) */
	u32 ttbcr;
	arm_cp15_ttbcr_get(ttbcr);
	ttbcr &= ~((1<<31) | 0b111);
	ttbcr |=   (0<<31  | 0b000);
	arm_cp15_ttbcr_set(ttbcr);
	
	/* set TTBR0's table's address */
	u32 ttbr0;
	arm_cp15_ttbr0_get32(ttbr0);
	ttbr0 &= ~(0xFFFFC000);
	ttbr0 |=  (((u32)_tt_l1) & 0xFFFFC000);
	arm_cp15_ttbr0_set32(ttbr0);

	/* set all MMU domains as "client" (translation table permission bits are checked) (see DDI0406C.D B3.12.3) */
	u32 dacr = 0b01010101010101010101010101010101;
	arm_cp15_dacr_set(dacr);

	arm_cp15_sync();

	/* clear level 1 translation table */
	mmu_tt_l1_clear();

	/* create section entry for kernel */
	mmu_tt_l1_mapsection(CONFIG_CODE_KERNEL_STARTADDR, CONFIG_CODE_KERNEL_STARTADDR);

	/* create section entry for IO devices */
	u32 iobase = 0x1C000000 & 0xFFF00000;  /* (see QEMU:vexpress.c) */
	mmu_tt_l1_mapsection(iobase, iobase);

	uart_println("enabling MMU...");

	/* enable MMU */
	arm_cp15_sctlr_get(sctlr);
	sctlr |= (1 << 0);
	arm_cp15_sctlr_set(sctlr);

	arm_cp15_sync();

	uart_println("... enable success!");
}

__attribute__ ((interrupt ("ABORT")))
void
_handler_abort(void) {
	uart_println("abort exception occurred! halting CPU...");
	for (;;) { arm_sleep(); }
}

static void
mmu_map() {
	/* see: DDI0406C.D figure B3-3 */
	/* see: DDI0406C.D figure B3.5.1 */
	/* see: DDI0406C.D figure B3-7 */
	/* see: DDI0406C.D figure B3-11 */
}

void
main(void) {
	uart_println("04_mmu started!");

	mmu_init();

	uart_println("trying to write to un-mapped memory...");
	(*((u32 *)0xF00FF00F)) = 1;

	uart_println("done");
	for (;;) { arm_sleep(); }
}
