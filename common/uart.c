/*
2020 David DiPaola
Licensed under CC0 (public domain)
*/

#include "./uart.h"

#include "./inttypes.h"

/* PL011 registers (see: DDI0183 3.2) */
struct pl011 {
        volatile u32 dr;  /* 0x000 */
};
static struct pl011 * const _uart0 = (void *)0x1C090000;  /* (see QEMU:vexpress.c) */

void
uart_print_char(char c) {
	_uart0->dr = c;
}

void
uart_print(const char * s) {
	char c;
	for (;;) {
		c = *s;
		if (c == '\0') break;
		uart_print_char(c);
		s++;
	}
}

void
uart_print_nl(void) {
	uart_print("\r\n");
}

void
uart_println(const char * s) {
	uart_print(s);
	uart_print_nl();
}

void
uart_print_hex32(u32 value) {
	uart_print("0x");
	for (int i=7; i>=0; i--) {
		u8 hex = ((value >> (i*4)) & 0xF);
		char hex_char = hex + ((hex < 0xA) ? '0' : ('A' - 0xA));
		uart_print_char(hex_char);
	}
}
