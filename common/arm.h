/*
2020 David DiPaola
Licensed under CC0 (public domain)
*/

#ifndef __ARM_H
#define __ARM_H

/* put CPU to sleep until an interrupt occurs */
#define arm_sleep() __asm("wfi")

/* CPU interrupt definitions */
#define arm_interrupt_enable() __asm("cpsie i")

/* SCTLR (System Control Register) definitions (see: DDI0406C.D B4.1.130) */
#define arm_cp15_sctlr_get(var) __asm("mrc p15, 0, %[__reg], c1, c0, 0" : [__reg] "=r" (var))
#define arm_cp15_sctlr_set(var) __asm("mcr p15, 0, %[__reg], c1, c0, 0" : : [__reg] "r" (var))

/* TTBCR (Translation Table Base Control Register) definitions (see: DDI0406C.D B4.1.153) */
#define arm_cp15_ttbcr_get(var) __asm("mrc p15, 0, %[__reg], c2, c0, 2" : [__reg] "=r" (var))
#define arm_cp15_ttbcr_set(var) __asm("mcr p15, 0, %[__reg], c2, c0, 2" : : [__reg] "r" (var))

/* TTBR0 (Translation Table Base Register 0) 32-bit definitions (see: DDI0406C.D B4.1.154) */
#define arm_cp15_ttbr0_get32(var) __asm("mrc p15, 0, %[__reg], c2, c0, 0" : [__reg] "=r" (var))
#define arm_cp15_ttbr0_set32(var) __asm("mcr p15, 0, %[__reg], c2, c0, 0" : : [__reg] "r" (var))

/* DACR (Domain Access Control Register) definitions (see: DDI0406C.D B4.1.43) */
#define arm_cp15_dacr_get(var) __asm("mrc p15, 0, %[__reg], c3, c0, 0" : [__reg] "=r" (var))
#define arm_cp15_dacr_set(var) __asm("mcr p15, 0, %[__reg], c3, c0, 0" : : [__reg] "r" (var))

/* ensures that changes made to CP15 are visible to the rest of the system (see: DDI0406C.D B3.15.5) */
#define arm_cp15_sync() __asm("isb")

#endif
