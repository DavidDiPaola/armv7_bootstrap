/*
2020 David DiPaola
Licensed under CC0 (public domain)
*/

#ifndef __INTTYPES_H
#define __INTTYPES_H

typedef unsigned char   u8;
typedef unsigned short u16;
typedef unsigned int   u32;

#endif
