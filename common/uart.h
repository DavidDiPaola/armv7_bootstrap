/*
2020 David DiPaola
Licensed under CC0 (public domain)
*/

#ifndef __UART_H
#define __UART_H

#include "./inttypes.h"

void
uart_print_char(char c);

void
uart_print(const char * s);

void
uart_print_nl(void);

void
uart_println(const char * s);

void
uart_print_hex32(u32 value);

#endif
