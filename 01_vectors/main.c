/*
2018,2019,2020 David DiPaola
Licensed under CC0 (public domain)
*/

#include "../common/uart.h"

__attribute__ ((interrupt ("SWI")))
void
_handler_svc(void) {
	uart_println("(step 2 of 3) SVC exception occurred");
}

void
main(void) {
	uart_println("01_vectors started!");

	uart_println("(step 1 of 3) going to trigger SVC exception...");
	__asm("svc 42");
	uart_println("(step 3 of 3) returned from SVC exception");
}
