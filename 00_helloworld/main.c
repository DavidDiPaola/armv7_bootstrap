/*
2018,2019,2020 David DiPaola
Licensed under CC0 (public domain)
*/

#include "../common/inttypes.h"

#include "../common/uart.h"

void
main(void) {
	uart_println("hello world!");
}
